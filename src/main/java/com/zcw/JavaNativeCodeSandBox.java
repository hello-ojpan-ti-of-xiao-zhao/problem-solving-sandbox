package com.zcw;

import com.zcw.model.ExecuteCodeRequest;
import com.zcw.model.ExecuteCodeResponse;
import com.zcw.template.JavaCodeSandBoxTemplate;
import org.springframework.stereotype.Component;

@Component
public class JavaNativeCodeSandBox extends JavaCodeSandBoxTemplate {

    @Override
    public ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest) {
        return super.executeCode(executeCodeRequest);
    }
}
