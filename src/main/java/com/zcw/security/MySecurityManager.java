package com.zcw.security;

import javax.sql.rowset.serial.SerialException;
import java.io.FileDescriptor;
import java.security.Permission;

/**
 * 细颗粒的权限检查
 */
public class MySecurityManager extends SecurityManager{

    /**
     * 检查所有的权限
     * @param perm
     */
    @Override
    public void checkPermission(Permission perm) {
        System.out.println("权限检查" + perm.toString());

    }

    /**
     *  检查程序是否允许执行权限
     * @param cmd
     */
    @Override
    public void checkExec(String cmd) {
        throw new SecurityException("checkExec 权限异常"+cmd);
    }

    @Override
    public void checkRead(String file) {

    }

    /**
     *  检查程序是否允许读程序
     * @param file
     */


    @Override
    public void checkWrite(String file) {
    }

    /**
     *  检查程序是否可以连接网络
     * @param host
     * @param port
     */
    @Override
    public void checkConnect(String host, int port) {
    }

    /**
     *  检测程序是否可以删除文件
     * @param file
     */
    @Override
    public void checkDelete(String file) {
    }
}
