package com.zcw;


import com.zcw.model.ExecuteCodeRequest;
import com.zcw.model.ExecuteCodeResponse;
import org.springframework.stereotype.Component;

/**
 *  代码沙箱接口
 */
@Component
public interface CodeSandBox {

        ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest);
}
